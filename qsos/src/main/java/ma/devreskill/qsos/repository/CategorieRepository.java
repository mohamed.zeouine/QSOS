package ma.devreskill.qsos.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import ma.devreskill.qsos.entity.Categorie;

@Component
public interface CategorieRepository extends CrudRepository<Categorie,Integer> {

}
