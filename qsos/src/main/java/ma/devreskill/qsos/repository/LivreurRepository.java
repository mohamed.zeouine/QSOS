package ma.devreskill.qsos.repository;

import ma.devreskill.qsos.entity.Livreur;

import org.springframework.data.repository.CrudRepository;

public interface LivreurRepository extends CrudRepository <Livreur, Integer> {

}
