package ma.devreskill.qsos.repository;

import org.springframework.data.repository.CrudRepository;

import ma.devreskill.qsos.entity.Client;

public interface ClientRepository extends CrudRepository<Client, Integer>{

}
