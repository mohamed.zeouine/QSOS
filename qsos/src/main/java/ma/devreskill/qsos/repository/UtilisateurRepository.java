package ma.devreskill.qsos.repository;

import org.springframework.data.repository.CrudRepository;

import ma.devreskill.qsos.entity.Utilisateur;

public interface UtilisateurRepository extends CrudRepository<Utilisateur, Integer>{
	public Utilisateur findByLogin(String login);
}
