package ma.devreskill.qsos.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import ma.devreskill.qsos.entity.Offre;

@Component
public interface OffreRepository extends CrudRepository<Offre,Integer>{

}