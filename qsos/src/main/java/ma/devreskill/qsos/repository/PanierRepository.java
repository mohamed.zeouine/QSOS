package ma.devreskill.qsos.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import ma.devreskill.qsos.entity.Panier;

@Component
public interface PanierRepository extends CrudRepository<Panier,Integer> {

}
