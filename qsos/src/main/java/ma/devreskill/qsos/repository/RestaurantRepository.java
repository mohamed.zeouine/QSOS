package ma.devreskill.qsos.repository;

import org.springframework.data.repository.CrudRepository;

import ma.devreskill.qsos.entity.Restaurant;

public interface RestaurantRepository extends CrudRepository<Restaurant, Integer>{

}
