package ma.devreskill.qsos.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Component;

import ma.devreskill.qsos.entity.Commande;

@Component
public interface CommandeRepository extends PagingAndSortingRepository<Commande, Integer>{

	public Iterable<Commande> findAllByClientById(Integer id );
	
	
}
