package ma.devreskill.qsos.qsos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QsosApplication {

public static void main(String[] args) {
		SpringApplication.run(QsosApplication.class, args);
	}
}
