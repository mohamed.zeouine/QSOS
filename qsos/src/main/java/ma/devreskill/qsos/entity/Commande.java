package ma.devreskill.qsos.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the commande database table.
 * 
 */
@Entity
@NamedQuery(name="Commande.findAll", query="SELECT c FROM Commande c")
public class Commande implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String modePaiement;

	private int noteCom;

	private int numero;

	//bi-directional many-to-one association to Client
	@ManyToOne
	@JoinColumn(name="Cli_id")
	private Client client;

	//bi-directional many-to-one association to Livreur
	@ManyToOne
	@JoinColumn(name="Liv_id")
	private Livreur livreur;

	//bi-directional one-to-one association to Etatcom
	@OneToOne(mappedBy="commande")
	private Etatcom etatcom;

	//bi-directional many-to-one association to Panier
	@OneToMany(mappedBy="commande")
	private List<Panier> paniers;

	public Commande() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getModePaiement() {
		return this.modePaiement;
	}

	public void setModePaiement(String modePaiement) {
		this.modePaiement = modePaiement;
	}

	public int getNoteCom() {
		return this.noteCom;
	}

	public void setNoteCom(int noteCom) {
		this.noteCom = noteCom;
	}

	public int getNumero() {
		return this.numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Livreur getLivreur() {
		return this.livreur;
	}

	public void setLivreur(Livreur livreur) {
		this.livreur = livreur;
	}

	public Etatcom getEtatcom() {
		return this.etatcom;
	}

	public void setEtatcom(Etatcom etatcom) {
		this.etatcom = etatcom;
	}

	public List<Panier> getPaniers() {
		return this.paniers;
	}

	public void setPaniers(List<Panier> paniers) {
		this.paniers = paniers;
	}

	public Panier addPanier(Panier panier) {
		getPaniers().add(panier);
		panier.setCommande(this);

		return panier;
	}

	public Panier removePanier(Panier panier) {
		getPaniers().remove(panier);
		panier.setCommande(null);

		return panier;
	}

}