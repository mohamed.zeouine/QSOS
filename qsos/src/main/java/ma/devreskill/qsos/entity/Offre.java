package ma.devreskill.qsos.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the offre database table.
 * 
 */
@Entity
@NamedQuery(name="Offre.findAll", query="SELECT o FROM Offre o")
public class Offre implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String description;

	private String nom;

	private String photo;

	private BigDecimal prix;

	private int res_id;

	//bi-directional many-to-many association to Categorie
	@ManyToMany(mappedBy="offres1")
	private List<Categorie> categories1;

	//bi-directional many-to-many association to Categorie
	@ManyToMany
	@JoinTable(
		name="categorieoffre"
		, joinColumns={
			@JoinColumn(name="Off_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id")
			}
		)
	private List<Categorie> categories2;

	//bi-directional one-to-one association to Restaurant
	@OneToOne
	@JoinColumn(name="id")
	private Restaurant restaurant;

	//bi-directional many-to-one association to Panier
	@OneToMany(mappedBy="offre")
	private List<Panier> paniers;

	public Offre() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPhoto() {
		return this.photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public BigDecimal getPrix() {
		return this.prix;
	}

	public void setPrix(BigDecimal prix) {
		this.prix = prix;
	}

	public int getRes_id() {
		return this.res_id;
	}

	public void setRes_id(int res_id) {
		this.res_id = res_id;
	}

	public List<Categorie> getCategories1() {
		return this.categories1;
	}

	public void setCategories1(List<Categorie> categories1) {
		this.categories1 = categories1;
	}

	public List<Categorie> getCategories2() {
		return this.categories2;
	}

	public void setCategories2(List<Categorie> categories2) {
		this.categories2 = categories2;
	}

	public Restaurant getRestaurant() {
		return this.restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public List<Panier> getPaniers() {
		return this.paniers;
	}

	public void setPaniers(List<Panier> paniers) {
		this.paniers = paniers;
	}

	public Panier addPanier(Panier panier) {
		getPaniers().add(panier);
		panier.setOffre(this);

		return panier;
	}

	public Panier removePanier(Panier panier) {
		getPaniers().remove(panier);
		panier.setOffre(null);

		return panier;
	}

}