package ma.devreskill.qsos.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the categorie database table.
 * 
 */
@Entity
@NamedQuery(name="Categorie.findAll", query="SELECT c FROM Categorie c")
public class Categorie implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String description;

	private String nom;

	//bi-directional many-to-many association to Offre
	@ManyToMany
	@JoinTable(
		name="categorieoffre"
		, joinColumns={
			@JoinColumn(name="id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="Off_id")
			}
		)
	private List<Offre> offres1;

	//bi-directional many-to-many association to Offre
	@ManyToMany(mappedBy="categories2")
	private List<Offre> offres2;

	public Categorie() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Offre> getOffres1() {
		return this.offres1;
	}

	public void setOffres1(List<Offre> offres1) {
		this.offres1 = offres1;
	}

	public List<Offre> getOffres2() {
		return this.offres2;
	}

	public void setOffres2(List<Offre> offres2) {
		this.offres2 = offres2;
	}

}