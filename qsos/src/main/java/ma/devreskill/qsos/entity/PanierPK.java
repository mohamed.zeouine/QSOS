package ma.devreskill.qsos.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the panier database table.
 * 
 */
@Embeddable
public class PanierPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private int off_id;

	@Column(insertable=false, updatable=false)
	private int id;

	public PanierPK() {
	}
	public int getOff_id() {
		return this.off_id;
	}
	public void setOff_id(int off_id) {
		this.off_id = off_id;
	}
	public int getId() {
		return this.id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PanierPK)) {
			return false;
		}
		PanierPK castOther = (PanierPK)other;
		return 
			(this.off_id == castOther.off_id)
			&& (this.id == castOther.id);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.off_id;
		hash = hash * prime + this.id;
		
		return hash;
	}
}