package ma.devreskill.qsos.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the livreur database table.
 * 
 */
@Entity
@NamedQuery(name="Livreur.findAll", query="SELECT l FROM Livreur l")
public class Livreur extends Personne implements Serializable {
	private static final long serialVersionUID = 1L;



	private int numero;

	//bi-directional many-to-one association to Commande
	@OneToMany(mappedBy="livreur")
	private List<Commande> commandes;

	//bi-directional one-to-one association to Personne
	@OneToOne
	@JoinColumn(name="id")
	private Personne personne;

	public Livreur() {
	}


	public int getNumero() {
		return this.numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public List<Commande> getCommandes() {
		return this.commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}

	public Commande addCommande(Commande commande) {
		getCommandes().add(commande);
		commande.setLivreur(this);

		return commande;
	}

	public Commande removeCommande(Commande commande) {
		getCommandes().remove(commande);
		commande.setLivreur(null);

		return commande;
	}

	public Personne getPersonne() {
		return this.personne;
	}

	public void setPersonne(Personne personne) {
		this.personne = personne;
	}

}