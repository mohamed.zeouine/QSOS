package ma.devreskill.qsos.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the utilisateur database table.
 * 
 */
@Entity
@NamedQuery(name="Utilisateur.findAll", query="SELECT u FROM Utilisateur u")
public class Utilisateur implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private byte enabled;

	private String login;

	private String mdp;

	//bi-directional many-to-many association to Authority
	@ManyToMany(mappedBy="utilisateurs")
	private List<Authority> authorities;

	//bi-directional one-to-one association to Personne
	@OneToOne(mappedBy="utilisateur1")
	private Personne personne1;

	//bi-directional one-to-one association to Personne
	@OneToOne
	@JoinColumn(name="id")
	private Personne personne2;

	public Utilisateur() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getEnabled() {
		return this.enabled;
	}

	public void setEnabled(byte enabled) {
		this.enabled = enabled;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMdp() {
		return this.mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public List<Authority> getAuthorities() {
		return this.authorities;
	}

	public void setAuthorities(List<Authority> authorities) {
		this.authorities = authorities;
	}

	public Personne getPersonne1() {
		return this.personne1;
	}

	public void setPersonne1(Personne personne1) {
		this.personne1 = personne1;
	}

	public Personne getPersonne2() {
		return this.personne2;
	}

	public void setPersonne2(Personne personne2) {
		this.personne2 = personne2;
	}

}