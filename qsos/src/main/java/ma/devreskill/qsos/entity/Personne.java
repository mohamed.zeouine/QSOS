package ma.devreskill.qsos.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the personne database table.
 * 
 */
@Entity
@NamedQuery(name="Personne.findAll", query="SELECT p FROM Personne p")
public class Personne implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String adresse;

	private String email;

	private String nom;

	private String prenom;

	private String tel;

	private int uti_id;

	//bi-directional one-to-one association to Client
	@OneToOne(mappedBy="personne")
	private Client client;

	//bi-directional one-to-one association to Livreur
	@OneToOne(mappedBy="personne")
	private Livreur livreur;

	//bi-directional one-to-one association to Utilisateur
	@OneToOne
	@JoinColumn(name="id")
	private Utilisateur utilisateur1;

	//bi-directional one-to-one association to Utilisateur
	@OneToOne(mappedBy="personne2")
	private Utilisateur utilisateur2;

	public Personne() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAdresse() {
		return this.adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public int getUti_id() {
		return this.uti_id;
	}

	public void setUti_id(int uti_id) {
		this.uti_id = uti_id;
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Livreur getLivreur() {
		return this.livreur;
	}

	public void setLivreur(Livreur livreur) {
		this.livreur = livreur;
	}

	public Utilisateur getUtilisateur1() {
		return this.utilisateur1;
	}

	public void setUtilisateur1(Utilisateur utilisateur1) {
		this.utilisateur1 = utilisateur1;
	}

	public Utilisateur getUtilisateur2() {
		return this.utilisateur2;
	}

	public void setUtilisateur2(Utilisateur utilisateur2) {
		this.utilisateur2 = utilisateur2;
	}

}