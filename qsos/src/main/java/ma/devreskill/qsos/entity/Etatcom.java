package ma.devreskill.qsos.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the etatcom database table.
 * 
 */
@Entity
@NamedQuery(name="Etatcom.findAll", query="SELECT e FROM Etatcom e")
public class Etatcom implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private int com_id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dateTime;

	private String status;

	//bi-directional one-to-one association to Commande
	@OneToOne
	@JoinColumn(name="id")
	private Commande commande;

	public Etatcom() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCom_id() {
		return this.com_id;
	}

	public void setCom_id(int com_id) {
		this.com_id = com_id;
	}

	public Date getDateTime() {
		return this.dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Commande getCommande() {
		return this.commande;
	}

	public void setCommande(Commande commande) {
		this.commande = commande;
	}

}