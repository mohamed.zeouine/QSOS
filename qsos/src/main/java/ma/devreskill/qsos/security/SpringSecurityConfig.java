package ma.devreskill.qsos.security;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import ma.devreskill.qsos.service.loginService;


@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
    private AuthenticationSuccessHandler successHandler;
	@Autowired
	private LogoutSuccessHandler logoutHandler;
	
	 @Autowired
	  private DataSource dataSource;
	 
	 @Autowired
	 private loginService loginService;
	 

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable()
                .authorizeRequests()
                .and()
                .formLogin().successHandler(successHandler).loginPage("/login")
                    .loginProcessingUrl("/login")
                    .failureUrl("/login/failed")
                    .permitAll()
                    .and()
                .logout().logoutSuccessHandler(logoutHandler)
					.permitAll();
    }
	 
	 
    @Bean
    public UserDetailsService userDetailsService() {
      return new loginService();
    };
    
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
      return new BCryptPasswordEncoder();
    };
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
      auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
    }

}