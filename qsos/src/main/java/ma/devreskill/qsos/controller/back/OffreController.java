package ma.devreskill.qsos.controller.back;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import ma.devreskill.qsos.entity.Offre;
import ma.devreskill.qsos.repository.OffreRepository;
import ma.devreskill.qsos.service.OffreService;

@Controller
public class OffreController {

	@Autowired
	private OffreService offreService;
	 @Autowired
    private OffreRepository offreRepository; 
	 @GetMapping ("/ListeOffres")
		public String listeOffre(Map<String, Object> model) {
			
			model.put("listOffres", offreService.findAll());
			
			return "listeoffres";
		}  
	 
		
		
		@GetMapping("/AjoutOffre")
		public String ajoutOffre(Map<String, Object> model) {					
			return "formoffre";
		}
		
		
		
	
		@GetMapping("/UpdateOffre{id}")
		public String updateOffre(Map<String, Object> model,@RequestParam  String id) {
			model.put("offre", offreRepository.findById(Integer.parseInt(id)).get());
			return "formoffre";
		}
		

		@GetMapping("/DeleteOffre{id}")
		public RedirectView deleteOffre(@RequestParam  int id) {
			offreService.delete(offreService.findById(id));
			return  new RedirectView("ListeOffres");
		}
		
		
	
		
		
		@PostMapping("/saveOffre")
		public RedirectView saveMarque(Offre offre) {	
			
			offreRepository.save(offre);
			
			return new RedirectView("ListeOffres");
		}
		

	 
	 
	 
	 
	 
}