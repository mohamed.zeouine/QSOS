package ma.devreskill.qsos.controller.back;
import java.util.Map;
import ma.devreskill.qsos.entity.Livreur;
import ma.devreskill.qsos.repository.LivreurRepository;
import ma.devreskill.qsos.service.LivreurService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class LivreurController  {
	 @Autowired
	   private LivreurService livreurService;

	   @Autowired
	   private LivreurRepository livreurRepository;


		@GetMapping("/AfficheLivreur")
	
		public String AfficheLivreur(Map<String, Object> model) {
		
			model.put("AfficheLivreur",livreurService.findAll());
			 
			return "AfficheLivreur";
		}
		
	       @GetMapping("/AjoutLivreur")
			
			public String AjoutLivreur(Map<String, Object> model) {
			
				return "AjoutLivreur";
			}
	       @PostMapping("saveLivreur")
	       public RedirectView saveLivreur(Livreur livreur){
	      	 livreurRepository.save(livreur);
	      	 return new RedirectView("AfficheLivreur");
	      	 
	       }
	       @GetMapping("UpdateLivreur")
			
			public String updateLivreur(Map<String, Object> model,@RequestParam int id) {
			
				model.put("livreur",livreurRepository.findById(id).get());
				
				return "AjoutLivreur";
			}
	       @GetMapping("DeleteLivreur")
			
			public RedirectView deleteLivreur(Map<String, Object> model,@RequestParam int id) {
			
				Livreur livreur=livreurRepository.findById(id).get();
				
				livreurRepository.delete(livreur);
				return  new RedirectView("AfficheLivreur");
			}

}
