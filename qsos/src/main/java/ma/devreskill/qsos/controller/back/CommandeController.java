package ma.devreskill.qsos.controller.back;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;

import ma.devreskill.qsos.service.CommandeService;

@Controller
public class CommandeController {
	
	
	
	@Autowired
	private CommandeService commandeService;
	
	
    @GetMapping("/listeCommandes")
    @Transactional(readOnly = true)
    public String welcome(Map<String, Object> model) {
    	model.put("commandes", commandeService.findAll());
		return null;
    	
    }
 
	
	

}
