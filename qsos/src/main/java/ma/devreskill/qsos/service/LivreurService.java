package ma.devreskill.qsos.service;

import ma.devreskill.qsos.entity.Livreur;

public interface LivreurService {
	public Iterable<Livreur> findAll();

	public Livreur findById(int id);


}
