package ma.devreskill.qsos.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import ma.devreskill.qsos.entity.Utilisateur;
import ma.devreskill.qsos.repository.UtilisateurRepository;

@Service
public class loginService implements UserDetailsService {
	
	@Autowired
	private  UtilisateurRepository utilisateurRepository;
	 
	@Override
	  public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		Utilisateur utilisateur = utilisateurRepository.findByLogin(login);

	    UserBuilder builder = null;
	    if (utilisateur != null) {
	      builder = org.springframework.security.core.userdetails.User.withUsername(login);
	      builder.password(new BCryptPasswordEncoder().encode(utilisateur.getMdp()));
	      builder.roles(utilisateur.getAuthorities().toString());
	    } else {
	      throw new UsernameNotFoundException("User not found.");
	    }

	    return builder.build();
	  }

	  
	}