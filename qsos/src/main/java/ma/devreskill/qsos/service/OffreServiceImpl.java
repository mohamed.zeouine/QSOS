package ma.devreskill.qsos.service;

import org.springframework.stereotype.Service;

import ma.devreskill.qsos.entity.Offre;
import ma.devreskill.qsos.repository.OffreRepository;

@Service
public class OffreServiceImpl implements OffreService {
   	
	private final OffreRepository offreRepository;

	public OffreServiceImpl(OffreRepository offreRepository) {
		super();
		this.offreRepository = offreRepository;
	}

	@Override
	public Iterable<Offre> findAll() {
		return this.offreRepository.findAll();
	}

	@Override
	public Offre findById(int id) {
		return this.offreRepository.findById(id).get();
	}

	@Override
	public void delete(Offre offre) {
		this.offreRepository.delete(offre);
		
	}

	@Override
	public void save(Offre offre) {
		this.offreRepository.save(offre);
		
	}
	
	

}
