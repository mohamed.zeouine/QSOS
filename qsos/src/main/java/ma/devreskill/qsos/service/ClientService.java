package ma.devreskill.qsos.service;

import java.util.Optional;

import ma.devreskill.qsos.entity.Client;


public interface ClientService {
	
	public Optional<Client> findByNomAndPrenom(String nom, String prenom);
	
	
}
