package ma.devreskill.qsos.service;

import ma.devreskill.qsos.entity.Offre;

public interface OffreService {
	    
	public Iterable<Offre> findAll();
	public Offre findById(int id);
	public void delete(Offre offre);
	public void save(Offre offre);

}
