package ma.devreskill.qsos.service;

import ma.devreskill.qsos.entity.Panier;

public interface PanierService {
	
	public Iterable<Panier> findAll();
	public void delete(Panier panier);
	public void save(Panier panier);

}
