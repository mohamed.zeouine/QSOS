package ma.devreskill.qsos.service;

import ma.devreskill.qsos.entity.Commande;

public interface CommandeService {
	
	public Iterable<Commande> findAll();
	public void delete(Commande commande);
	public void save(Commande commande);
	public Commande findById(int id);

}
