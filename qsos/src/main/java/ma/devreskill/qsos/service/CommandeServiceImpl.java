package ma.devreskill.qsos.service;



import ma.devreskill.qsos.entity.Commande;

import ma.devreskill.qsos.repository.CommandeRepository;

import ma.devreskill.qsos.entity.Commande;
import ma.devreskill.qsos.repository.CommandeRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;



@Component("commandeService")
@Transactional
public class CommandeServiceImpl implements CommandeService {

	@Override
	public Iterable<Commande> findAll() {
		
		return this.commandeRepository.findAll();
	}

	private final CommandeRepository commandeRepository;
	
	public CommandeServiceImpl(CommandeRepository commandeRepository){
		this.commandeRepository = commandeRepository;
	}
	@Override
	public void delete(Commande commande) {
		this.commandeRepository.delete(commande);
	}
	@Override
	public void save(Commande commande) {
		this.commandeRepository.save(commande);
		
	}
	@Override
	public Commande findById(int id) {
		return this.commandeRepository.findById(id).get();
	}
	
}
