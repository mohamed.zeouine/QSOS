package ma.devreskill.qsos.service;

import java.util.Optional;

import org.springframework.stereotype.Service;


import ma.devreskill.qsos.entity.Restaurant;
import ma.devreskill.qsos.repository.RestaurantRepository;


@Service
public class RestaurantServiceImpl implements RestaurantService{
	
	private final RestaurantRepository restaurantRepository;
	
	public RestaurantServiceImpl(RestaurantRepository restaurantRepository) {
		super();
		this.restaurantRepository = restaurantRepository;
	
	}

	@Override
	public Iterable<Restaurant> findAll() {
		// TODO Auto-generated method stub
		return this.restaurantRepository.findAll();
	}

	@Override
	public void save(Restaurant restaurant) {
		// TODO Auto-generated method stub
		this.restaurantRepository.save(restaurant);
	}

	@Override
	public Optional<Restaurant> findbyId(Integer id) {
		// TODO Auto-generated method stub
		return this.restaurantRepository.findById(id);
	}

	@Override
	public void delete(Restaurant restaurant) {
		// TODO Auto-generated method stub
		this.restaurantRepository.delete(restaurant);
		
	}
}
