package ma.devreskill.qsos.service;

import org.springframework.stereotype.Service;

import ma.devreskill.qsos.entity.Categorie;
import ma.devreskill.qsos.repository.CategorieRepository;

@Service
public class CategorieServiceImpl implements CategorieService {

	private final CategorieRepository categorieRepository;
	
	public CategorieServiceImpl(CategorieRepository categorieRepository) {
		super();
		this.categorieRepository = categorieRepository;
	}

	@Override
	public Iterable<Categorie> findAll() {
		return this.categorieRepository.findAll();
	}

	@Override
	public void delete(Categorie categorie) {
		this.categorieRepository.delete(categorie);
	}

	@Override
	public void save(Categorie categorie) {
		this.categorieRepository.save(categorie);
		
		
	}

	@Override
	public Categorie findById(int id) {
		
		return this.categorieRepository.findById(id).get();
	}

}
