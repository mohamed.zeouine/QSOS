package ma.devreskill.qsos.service;

import org.springframework.stereotype.Service;

import ma.devreskill.qsos.entity.Panier;
import ma.devreskill.qsos.repository.PanierRepository;

@Service
public class PanierServiceImpl implements PanierService {

	private final PanierRepository panierRepository;
	
	public PanierServiceImpl(PanierRepository panierRepository) {
		super();
		this.panierRepository = panierRepository;
	}

	@Override
	public Iterable<Panier> findAll() {
		return this.panierRepository.findAll();
	}

	@Override
	public void delete(Panier panier) {
		this.panierRepository.delete(panier);
	}

	@Override
	public void save(Panier panier) {
		this.panierRepository.save(panier);
		
		
	}



}
