package ma.devreskill.qsos.service;

import java.util.Optional;

import ma.devreskill.qsos.entity.Restaurant;

public interface RestaurantService {
	
	public Iterable<Restaurant> findAll();
	
	public void save(Restaurant restaurant);
	
	public Optional<Restaurant> findbyId(Integer id);
	
	public void delete(Restaurant restaurant);



}
