package ma.devreskill.qsos.service;

import ma.devreskill.qsos.entity.Categorie;

public interface CategorieService {
	
	public Iterable<Categorie> findAll();
	public void delete(Categorie categorie);
	public void save(Categorie categorie);
	public Categorie findById(int id);
}
